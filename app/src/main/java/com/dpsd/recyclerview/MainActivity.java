package com.dpsd.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recycler_view;
    StudentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler_view = findViewById(R.id.recycler_view);
        setRecyclerView();
    }

    private void setRecyclerView() {
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        adapter= new StudentAdapter(this,getList());
        recycler_view.setAdapter(adapter);
    }

    private List<Student> getList(){
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        studentList.add(new Student("1","Maurice","94.5"));
        studentList.add(new Student("2","Patrick","95"));
        studentList.add(new Student("3","Ingwe","100"));
        studentList.add(new Student("4","Shema","65"));
        return studentList;
    }
}