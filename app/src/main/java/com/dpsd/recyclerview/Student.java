package com.dpsd.recyclerview;

public class Student {
    String id;
    String name;
    String marks;

    public Student(String id, String name, String marks) {
        this.id = id;
        this.name = name;
        this.marks = marks;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMarks() {
        return marks;
    }
}
